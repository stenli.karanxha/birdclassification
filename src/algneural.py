################################################################
#  Neural network machine learning algorithm 
################################################################
import numpy as np
import math
import random
from sample import Sample
import csv
import mathutils

class NeuralNetwork(object):
    """
            Neural network machine learning algorithm, for a multi-class classification.
    """
    def __init__(self, num_input, num_hidden, num_output, learning_rate=1):
        """ Initialises neural network.
        Takes as input arguments the number of input-, hidden- and output-units. 
        Initialises number of units, activity of nodes and the weights (according to a uniform random distribution).
        """
        # initialise number of nodes        
        self.num_input = num_input + 1 #bias
        self.num_hidden = num_hidden
        self.num_output = num_output

        #initialise nodes 
        self.input_nodes = nodes_init(self.num_input)	
        self.hidden_nodes = nodes_init(self.num_hidden)
        self.output_nodes = nodes_init(self.num_output)

        #initialise weights (with random numbers)
        self.input_weights = weight_init(self.num_input, self.num_hidden)
        self.output_weights = weight_init(self.num_hidden, self.num_output)

        #initialise learning rate
        self.learning_rate = learning_rate	

    def train(self,  _train_data):
        """ Train the machine learning algorithm. 
            Input 
            _train_data -- list of Samples
        Method used is the steepest gradient descent. 
        """
        for sample in _train_data:
            self._change_activity(sample)
            self._change_weights(sample)

    def evaluate(self,  _test_data):
        """
            Evaluate test data, using the trained algorithm.
            Input: 
                _test_data -- list of Sample 
        """
        for index, sample in enumerate(_test_data):
            self._change_activity(sample)
            sample.set_classification(self.output_nodes)

    def _change_activity(self, sample):
        """ Updates the activities given an input sample.

        Takes sample as argument."""
        #update input unit activity
        self.input_nodes[1:] = sample.get_data().flatten() #think of bias 

        #update hidden unit activity
        for i in range(self.num_hidden):
            self.hidden_nodes[i] = np.dot(self.input_weights[:,i],self.input_nodes) #weights*activity

        #update output unit activity using a logistic neuron
        for i in range(self.num_output):
            self.output_nodes[i] = mathutils.sigmoid_scalar(np.dot(self.output_weights[:,i],self.hidden_nodes))

    def _change_weights(self,sample):
        """ Updates the weights after one training sample according to the backpropagation algorithm.
        Takes target value for this sample as input"""

        # learning rule: delta(w_i) = -epsilon(dError/dw_i) = sum_n(epsilon* x_i_n* y_n* (1-y_n)* (y_n-t_n))
        # --> the change in the weights corresponds to the learning rate times the derivative of the error (squared error) over the weight
        # --> neuron.delta = neuron.output(1-neuron.output) * error_factor 

        #compute error factors delta of output units
        errorfactor_output = sample.classification-self.output_nodes
        #compute error delta of output units
        delta_output = self.output_nodes*(1-self.output_nodes)*errorfactor_output	

        #compute error factors of hidden units by summing errorfactor(xi) = sum_j(wij*delta(xj))
        errorfactor_hidden = np.zeros(self.num_hidden)
        for i in range(self.num_hidden):
            errorfactor_hidden[i] = np.dot(self.output_weights[i,:],delta_output)
        #compute error delta of hidden units
        delta_hidden = self.hidden_nodes*(1-self.hidden_nodes)*errorfactor_hidden

        #new weight  = old weight +  learning rate * 1 * output of input neuron * delta	
        #compute change in input weights
        for i in range(self.num_input):
            for j in range(self.num_hidden):
                self.input_weights[i][j] = self.input_weights[i][j] + self.learning_rate*1*delta_hidden[j]

        #compute change in output weights
        for i in range(self.num_hidden):
            for j in range(self.num_output):
                self.output_weights[i][j] = self.output_weights[i][j] + self.learning_rate*1*delta_output[j]

####################################################################
#	Utility functions for the algorithm
####################################################################
def weight_init(rows,cols):
	""" Initialises weights randomly according to a uniform distribution."""
	# found as standard proceedure https://www.elen.ucl.ac.be/Proceedings/esann/esannpdf/es2001-6.pdf
	return np.random.uniform(-0.05,0.05,(rows,cols))

def nodes_init(length):
	""" Initialises node activity with 1."""
	return np.ones(length)   

###################################################
#	Test script
###################################################
def max_data(_train_data):
	""" Determines maximum size of train data samples.

	Returns size of matrix as integer (=rows*columns)."""
	max_input = 0
	for sample in _train_data:
		if max_input < np.size(sample.data):
			max_input = np.size(sample.data)
	return max_input
	

def equalize_data(data, input_size):
	""" Equalizes data such that it can be fed in the neural network.

	Takes data as input and fills up to desired length by randomly repeating
	existing samples."""
	for sample in data:
		#if data too "short"
		if np.size(sample.data) < input_size:		
			while np.size(sample.data) < input_size:
				column = random.randint(0,sample.data.shape[1]-1)
				sample.data = np.c_[sample.data,sample.data[:,column]]
		#if data too short, applies only to testing data
		else:
			n_columns = input_size/17
			sample.data = sample.data[:,:n_columns]
	return data

def create_output_csv(_test_data, name):
	""" Creates a CSV file with the output of the neural network for the test data.

	Takes evaluated test_data as input and creates a CSV file with the 1. column saying 
	the testfile-ID and the species and the second number giving the probability for that
	species computed by the neural network. """
	
	output = open(name,"wb")
	c = csv.writer(output)
	c.writerow(["ID","Probability"])	
	for index,sample in enumerate(_test_data,1):
		ID = sample.get_sample_id()+"_classnumber_"+str(index)
		for classification in sample.get_all_classifications():
			probability = classification 
			c.writerow([ID,probability])
	output.close()
	return output

def main():
	train_data = []
	test_data = []
	#generate fake data
	for i in range(10):	
		train_data.append(Sample(np.random.uniform(-0.5,0.5,(17,10)),str(i),np.random.randint(0,2,87),10))
		train_data.append(Sample(np.random.uniform(-0.5,0.5,(17,8)),str(i),np.random.randint(0,2,87),10))		
		test_data.append(Sample(np.random.uniform(-0.5,0.5,(17,10)),str(i),np.random.randint(0,2,87),10))	
		test_data.append(Sample(np.random.uniform(-0.5,0.5,(17,12)),str(i),np.random.randint(0,2,87),10))
	#determine input size
	input_size = max_data(train_data)		
	output_size = 87
	hidden_size = 30
	#create network and equalize data
	network_test = NeuralNetwork(input_size,hidden_size,output_size)	
	train_data = equalize_data(train_data, input_size)
	test_data = equalize_data(test_data,input_size)
	#train network, evaluate network, create csv-output file	
	network_test.train(train_data)
	network_test.evaluate(test_data)
	output = create_output_csv(test_data, "algneural_output.csv")	
	
if __name__ == '__main__':
	main()
