################################################################
#  Folders and files constants
################################################################

WINDOWS = None

if WINDOWS:
    MAIN_FOLDER = './Data'
    DIR_SEP = '\\'
else:
    MAIN_FOLDER = 'Data'
    DIR_SEP = '/'

INPUT_FOLDER = MAIN_FOLDER + DIR_SEP + 'Input'
LABELS_FILE_NAME = INPUT_FOLDER + DIR_SEP + 'numero_file_train.csv'
TRAIN_FOLDER = INPUT_FOLDER + DIR_SEP + 'Train'
TEST_FOLDER = INPUT_FOLDER + DIR_SEP + 'Test'
TRAIN_FILE_BASE_NAME = TRAIN_FOLDER + DIR_SEP + 'cepst_conc_cepst_nips4b_birds_trainfile'
TEST_FILE_BASE_NAME = TEST_FOLDER + DIR_SEP + 'cepst_conc_cepst_nips4b_birds_testfile'
    
SAMPLE_FILES_EXT = '.txt'

TRAIN_ID_BASE = 'cepst_conc_cepst_nips4b_birds_trainfile'
TEST_ID_BASE = 'cepst_conc_cepst_nips4b_birds_testfile'
ID_EXTENSION = '.wav_classnumber_'

INPUT_ROWS_NUMBER = 17

OUTPUT_FOLDER = MAIN_FOLDER + DIR_SEP + 'Output'
DUMMY_OUT_FILE_NAME = OUTPUT_FOLDER + DIR_SEP + 'random_submission.csv'
LINEAR_REGRESSION_OUT_FILE_NAME = OUTPUT_FOLDER + DIR_SEP + 'linear_regression_submission.csv'
NEURAL_NETWORKS_OUT_FILE_NAME = OUTPUT_FOLDER + DIR_SEP + 'neural_networks_submission.csv'
K_NEAREST_NEIGHBOURS_OUT_FILE_NAME = OUTPUT_FOLDER + DIR_SEP + 'k_nearest_neighbours_submission.csv'

OUTPUT_FILE_SEPARATOR = ';'

CLASSES_NUMBER = 87

TRAIN_SAMPLES_NUMBER = 687
TEST_SAMPLES_NUMBER = 1000

PERCENT_FOR_LEARNING = 75   # % of the training set used for the learning. The rest goes to the verification.

################################################################
#  Global data
################################################################
classifications = None
train_samples = None
learn_samples = None
verif_samples =  None
test_samples = None
num_features = 0

################################################################
#  I/O and utility functions
################################################################
from sample import Sample
import os.path
import numpy

def get_train_file_name(index):
    return TRAIN_FILE_BASE_NAME + str(index).zfill(3) + SAMPLE_FILES_EXT

def get_train_sample_id(index):
    return TRAIN_ID_BASE + str(index).zfill(3) + ID_EXTENSION

def get_test_file_name(index):
    return TEST_FILE_BASE_NAME + str(index).zfill(4) + SAMPLE_FILES_EXT

def get_test_sample_id(index):
    return TEST_ID_BASE + str(index).zfill(4) + ID_EXTENSION

def read_cepst_file(_file_name):
    """
        Reads the data of a single sample from a text file.
        The data are there represented as a matrix with 17 lines.
    """
    if not os.path.exists(_file_name):
        return None
    rv = []
    with open(_file_name, 'r') as file:
        for line in file:
            words = line.split()
            row = [float(word) for word in words]
            rv.append(row)
    if len(rv) == INPUT_ROWS_NUMBER:
        return rv
    else:
        return None

def prepare_classification_data():
    """
        Reads and transforms into a dictionary the file of the 
        classifications for the train data.
    """
    if not os.path.exists(LABELS_FILE_NAME):
        return False
    global classifications
    classifications = {}
    with open(LABELS_FILE_NAME, 'r') as file:
        for line in file:
            words = line.split(',')
            row = [float(word) for word in words]
            if len(row) == CLASSES_NUMBER + 2:
                file_id = int(row[0])
                if not file_id in classifications:
                    classifications[file_id] = row[1:CLASSES_NUMBER + 2]
    print 'Completed prepare_classification_data()'
    return True

def prepare_train_data():
    """
        Prepares the training data:
        Reads the training files, and prepares a list of Sample objects, 
        each containing the data, the duration of the data and the 
        classification.
    """
    global train_samples
    train_samples = []
    for i in range(1, TRAIN_SAMPLES_NUMBER+1):
        sample_data = read_cepst_file(get_train_file_name(i))
        if sample_data and len(sample_data) > 0 and i in classifications:
            sample_id = get_test_sample_id(i)
            probabilities = classifications[i][0:CLASSES_NUMBER]
            duration = classifications[i][CLASSES_NUMBER]
            my_sample = Sample(sample_data, sample_id, probabilities, duration)
            if my_sample.is_valid():
                train_samples.append(my_sample)
    print len(train_samples),  ' train samples read.'
    return len(train_samples) > 0
    
def prepare_test_data():
    """
        Prepares the test data.
        Reads the test files, and prepares a list of Sample objects, 
        each containing the data and the duration. Obviously 
        no classification information is initialized, as it will be 
        provided from the evaluation part of the single algorithms.
    """
    global test_samples
    test_samples = []
    for i in range(1,  TEST_SAMPLES_NUMBER+1):
        sample_data = read_cepst_file(get_test_file_name(i))
        if sample_data and len(sample_data) > 0:
            sample_id = get_test_sample_id(i)
            probabilities = [0 for i in range(CLASSES_NUMBER)]
            duration = classifications[i][CLASSES_NUMBER]
            my_sample = Sample(sample_data, sample_id,  probabilities,  duration)
            if my_sample.is_valid():
                test_samples.append(my_sample)
    print len(test_samples),  ' test samples read.'
    return len(test_samples) > 0
    
def calc_number_of_inputs():
    """
        Calculates the maximum length of the data,  taking into 
        account the training and test samples.
    """
    global num_features
    num_features = 0
    for my_sample in train_samples:
        num_features = max([num_features,  my_sample.get_data_length()])
    for my_sample in test_samples:
        num_features = max([num_features,  my_sample.get_data_length()])
    print 'Completed calc_number_of_inputs()'
    return True

def pad_sample_data():
    """
        All the input data should have the same length, given by
        the maximum length, as the implemented algorithm can 
        not deal properly with different length inputs.
    """
    global train_samples,  test_samples
    for my_sample in train_samples:
        my_sample.pad_data_to_length(num_features)
    for my_sample in test_samples:
        my_sample.pad_data_to_length(num_features)
    print 'Completed pad_sample_data()'
    return True

def prepare_learn_data():
    """
        Prepare the learning data of the algorithm, as 
        a partial list of the training set.
    """
    global learn_samples
    if PERCENT_FOR_LEARNING == 100:
        learn_samples = train_samples
    else:
        last_index = len(train_samples) * PERCENT_FOR_LEARNING / 100
        learn_samples = train_samples[0:last_index]
    print 'Filled the learning set with ',  len(learn_samples),  ' elements.'
    return True

def prepare_verification_data():
    """
        Prepare the verification data of the algorithm, as 
        a partial list of the training set.
    """
    global verif_samples
    if PERCENT_FOR_LEARNING == 100:
        verif_samples = []
    else:
        first_index = len(train_samples) * PERCENT_FOR_LEARNING / 100
        verif_samples = train_samples[first_index:-1]
    print 'Filled the verification set with ',  len(verif_samples),  ' elements.'
    return True

def print_all_data():
    """
        Debugging function.
        Prints out all the training and verification data.
    """
    print '--- Training samples start: \n'
    for my_sample in train_samples:
        print str(my_sample) + '\n'
    print '--- Training samples end: \n'
    print '--- Test samples start: \n'
    for my_sample in test_samples:
        print str(my_sample) + '\n'
    print '--- Test samples end: \n'

def save_test_samples(_file_name):
    """
        Writes the data in the output file: 
        csv file, with a header line, and two columns: 
        The I column has the information on the test file involved, and the class name
        The II  column contains the probability assigned to the specific class
    """
    with open(_file_name, 'w') as file:
        header = 'ID' + OUTPUT_FILE_SEPARATOR + 'Probability \n'
        file.write(header)
        for my_sample in test_samples:
            all_rows = my_sample.serialize(OUTPUT_FILE_SEPARATOR)
            for row in all_rows:
                file.write(row)
    
def clean_verification_samples():
    """
        Cleans the evaluated classifications from the verification samples, 
        allowing them to be re-used for another algorithm.
    """
    global verif_samples
    for my_sample in verif_samples:
        my_sample.reset_classification_to_original()
    
def clean_test_samples():
    """
        Cleans the evaluated classifications from the test samples, 
        allowing them to be re-used for another algorithm.
    """
    global test_samples
    for my_sample in test_samples:
        my_sample.reset_classification_to_original()

def eval_verification_samples():
    """
        Single number evaluation of the algorithm:
        as the average of the evaluations of each 
        verification sample.
    """
    vote_array = numpy.array([my_sample.evaluate_classification() for my_sample in verif_samples])
    return vote_array.mean()


################################################################
#  Run a generic algorithm, with the following steps: learn, verify, evaluate test data, save output
################################################################
def run_algorithm(_algorithm,  _file_name, _algorithm_name):
    """
        Run a single algorithm using the common interface for the steps of the execution.
    """
    print '----',  _algorithm_name, '----'
    
    _algorithm.train(train_samples)
    print 'Completed the training.'
    
    _algorithm.evaluate(verif_samples)
    print 'Completed the classification of the verification samples.'
    
    print 'The verification result is:',  eval_verification_samples()
    clean_verification_samples()

    _algorithm.evaluate(test_samples)
    print 'Completed the classification of the test samples.'
   
    save_test_samples(_file_name)
    clean_test_samples()
    print 'Completed writing the output of the classification.'


################################################################
#  Run
################################################################
from algdummy import Dummy
from alglinear import LinearRegression
from algneural import NeuralNetwork
from algneighbours import KNearestNeighbour

if __name__ == '__main__':
    """
        Read the training and test data.
        Run the different machine learning algorithms.
    """
    if prepare_classification_data() and prepare_train_data() and prepare_test_data() and \
        calc_number_of_inputs() and pad_sample_data() and \
        prepare_learn_data() and prepare_verification_data():
        
        #print_all_data()
        run_algorithm(Dummy(),  DUMMY_OUT_FILE_NAME,  'Dummy')
        run_algorithm(LinearRegression(num_features,  CLASSES_NUMBER),  LINEAR_REGRESSION_OUT_FILE_NAME,  'Linear Regression')
        un_algorithm(NeuralNetwork(num_features,30,CLASSES_NUMBER),  NEURAL_NETWORKS_OUT_FILE_NAME,  'Neural Network')
        run_algorithm(KNearestNeighbour(4),  K_NEAREST_NEIGHBOURS_OUT_FILE_NAME,  'K-nearest neighbours')
