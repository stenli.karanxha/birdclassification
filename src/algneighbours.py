################################################################
#  K nearest neighbour machine learning algorithm
################################################################

import numpy as np
from sample import Sample
import heapq as hq

class KNearestNeighbour(object):
    """
            K Nearest Neighbor machine learning algorithm, for a multi-class classification.
    """
    def __init__(self,_k,_normalization=0,_soft_bounds=0,_pca_enabled=0,_distance_metric='dist'):
        self.k = _k
        self.normalization = _normalization
        self.soft_bounds = _soft_bounds
        self.pca_enabled = _pca_enabled
        self.library = []
        self.train_labels = []
        self.max_size = 0
        self.num_classes = 0
        self.distance_metric = _distance_metric
        self.eigen_vectors = []
        self.reduced_dim_lib = []
        
    
    def train(self,  _train_data):
        """
            Train the machine learning algorithm. 
            Input: 
                _train_data -- list of Sample 
        """
        for sp in _train_data:
            if(self.max_size < sp.get_data_length()):
                self.num_classes = sp.get_num_classes()
                self.max_size = sp.get_data_length()

        for sp in _train_data:
            sp.pad_data_to_length(self.max_size)
            if(len(self.library)==0):
                row_data = np.array(sp.get_data().reshape(1,self.max_size))
                if(self.normalization):
                    row_data = (row_data-np.mean(row_data))/np.std(row_data) #np.linalg.norm(row_data)
                self.library = np.vstack(row_data)
                self.train_labels = np.vstack([sp.get_classification().argmax()])
            else:
                row_data = np.array(sp.get_data().reshape(1,self.max_size))
                if(self.normalization):
                    row_data = (row_data-np.mean(row_data))/np.std(row_data)#np.linalg.norm(row_data)
                self.library = np.vstack([ self.library, row_data])
                self.train_labels = np.vstack([self.train_labels,[sp.get_classification().argmax()]])
        if(self.pca_enabled):
            self._trainPCA()
            self.library = self._projPCA(self.library)
        
    def evaluate(self,  _test_data):
        """
            Evaluate test data, using the trained algorithm.
            Input: 
                _test_data -- list of Sample 
        """
        for t_data in _test_data:
            col_data = np.array(t_data.get_data().reshape(self.max_size,1))
            if(self.normalization):
                col_data = (col_data-np.mean(col_data))/np.std(col_data) #np.linalg.norm(col_data)
            if(self.pca_enabled):
                col_data = self._projPCA(col_data)
            rankings = self._distance(col_data)
            max_inds = [t[0] for t in hq.nlargest(self.k, enumerate(rankings), lambda t: t[1])]
            candidates = self.train_labels[max_inds]
            classification = np.zeros(self.num_classes)
            if(self.soft_bounds):
                counts = np.bincount(np.transpose(candidates).tolist()[0])
                probs = counts/float(sum(counts))
                classification[range(probs.size)] = classification[range(probs.size)] + probs
            else:
                assigned_class = np.argmax(np.bincount(np.transpose(candidates).tolist()[0])) #class that takes the maximum vote is assigned
                classification[assigned_class] = 1.0
            t_data.set_classification(classification)

    def _distance(self, data):
        if(self.distance_metric is 'corr'):
            return np.dot(self.library,data)
        elif(self.distance_metric is 'dist'):
            mat_data = np.tile(np.transpose(data),(self.library.shape[0],1))
            diff = self.library - mat_data
            int_ranks = np.apply_along_axis(np.linalg.norm,axis=1,arr=diff)
            return np.tile(max(int_ranks),int_ranks.shape)-int_ranks
        elif(self.distance_metric is 'blck_dist'):
            mat_data = np.tile(np.transpose(data),(self.library.shape[0],1))
            diff = abs(self.library - mat_data)
            int_ranks = np.apply_along_axis(np.sum,axis=1,arr=diff)
            return np.tile(max(int_ranks),int_ranks.shape)-int_ranks
            
            
    def _trainPCA(self,frac_inf = 0.8):
        new_array_rank=4
        print self.library.shape
        cov_pca = np.cov(self.library.T)
        
        eigen_values, eigen_vectors = np.linalg.eig(cov_pca)
        new_index = np.argsort(eigen_values)[::-1]
        eigen_vectors = eigen_vectors[:,new_index]
        eigen_values = eigen_values[new_index]
        eigen_vectors = eigen_vectors[:,:new_array_rank]
        self.eigen_vectors = eigen_vectors
    
    def _projPCA(self,data):
        return np.dot(self.eigen_vectors.T, data.T).T
