################################################################
#  Dummy machine learning algorithm
################################################################
import numpy

class Dummy(object):
    """
            Dummy machine learning algorithm, for a multi-class classification, which
        assigns to each class a probability of 0.5.
            Useful as a benchmark for other algorithms and also to test the correct 
        structure of the I/O.
    """
    def __init__(self):
        pass
    
    def train(self,  _train_data):
        """
            Train the machine learning algorithm. 
            Input: 
                _train_data -- list of Sample 
        """
        pass
        
    def evaluate(self,  _test_data):
        """
            Evaluate test data, using the trained algorithm.
            Input: 
                _test_data -- list of Sample 
            Output:
                _test_data -- with a changed classification array.
        """
        for my_sample in _test_data:
            my_classifications = [numpy.random.random_sample() for x in range(my_sample.get_num_classes())]
            my_sample.set_classification(my_classifications)
        
