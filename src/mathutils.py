####################################################################
#	Common utility functions for the different algorithms
####################################################################
import numpy

def sigmoid_scalar(_x):
    ''' Computes the sigmoid function of a scalar.'''
    if _x < -10:
        return 0.0
    elif _x > 10:
        return 1.0
    else:
        return 1.0 / (1.0 + numpy.exp(-_x))

def sigmoid(_numpyArray):
    return 1.0 / (1.0 + numpy.exp(-_numpyArray))
