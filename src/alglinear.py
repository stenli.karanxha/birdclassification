################################################################
#  Linear regression machine learning algorithm
################################################################
import numpy as np
import scipy.optimize
import mathutils
from sample import Sample

class LinearRegression(object):
    """
            Linear regression machine learning algorithm, for a multi-class classification.
    """
    def __init__(self, _num_features,   _num_classes,  _regularization = 0.0):
        """
        Constructor with arguments:
            _num_features: Number of components of the data
            _num_classes: Number of classification classes
            _regularization: Regularization parameter
        """
        self.num_features = _num_features
        self.num_classes = _num_classes
        self.parameters = np.zeros((self.num_features + 1,  self.num_classes))
        self.regularization = _regularization
    
    def train(self,  _train_data):
        """
            Train the machine learning algorithm. 
            Input: 
                _train_data -- list of Sample 
        """
        # Matrix with all the training samples: Each row contains a sample. 
        # The number of columns is given by the number of features. 
        X = np.array([_get_flat_biased_data(my_sample) for my_sample in _train_data])
        # Matrix with the expected outputs. Each row contains an output.
        # The number of columns is given by the number of classes.
        y = np.array([_get_flat_classification(my_sample) for my_sample in _train_data])
        # Uses a downhill simplex optimization, to calculate the optimal value of the parameters.
        #print _get_cost(self.parameters, X,  y,  self.regularization)
        try:
            self.parameters = scipy.optimize.fmin(_get_cost, self.parameters, \
                                                        args=(X,  y,  self.regularization),  maxiter=10)
        except:
            pass

    def evaluate(self,  _test_data):
        """
            Evaluate test data, using the trained algorithm.
            Input: 
                _test_data -- list of Sample 
        """
        for my_sample in _test_data:
            data = _get_flat_biased_data(my_sample)
            my_classification = np.dot(data,  self.parameters)
            my_norm_classification = mathutils.sigmoid(my_classification)
            my_sample.set_classification(my_norm_classification)

####################################################################
#	Utility functions for the algorithm
####################################################################
def _get_flat_biased_data(_sample):
    data = np.array(_sample.get_data().flatten())
    biased_data = np.concatenate(([1],data))
    return biased_data
    
def _get_flat_classification(_sample):
    rv = np.array(_sample.get_classification())
    return rv

def _get_cost(_parameters,  _X,  _y,  _regularization):
    '''
       Calculate the cost function with regularization.
    '''
    #print _X.shape
    #print _y.shape
    num_terms = _y.size
    
    # Cost without regularization
    y_eval = mathutils.sigmoid(_X.dot(_parameters))
    terms = -_y * np.log(y_eval) - (1 - _y) * np.log(1 - y_eval)
    cost = (1.0 / num_terms) * terms.sum()
   
    # Regularization component
    parameters_sqr = _parameters * _parameters
    parameters_sqr[0] = 0
    cost_reg = (_regularization / (2.0 * num_terms)) * parameters_sqr.sum()
    
    cost = cost + cost_reg
    print cost
    return cost

def _get_gradient(_parameters,  _X,  _y,  _regularization):
    '''
       Calculate the gradient with regularization.
    '''
    num_samples = _y.shape[0]
    
    # Gradient
    y_eval = mathutils.sigmoid(_X.dot(_parameters))
    gradient = (1.0 /num_samples) * ((_X.T).dot(y_eval - _y))
    
    # Regularized gradient
    gradient_reg = (_regularization / len(_y)) * _parameters
    gradient_reg[0] = 0
    
    gradient = gradient + gradient_reg
    
    return gradient
