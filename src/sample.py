################################################################
#  Sample class
################################################################
import numpy
import math

class Sample(object):
    """
        Sample and corresponding multiclass classification.
    """
    
    def __init__(self, _data, _sample_id, _classification = [], _duration = 0.0):
        """
        Constructor with arguments:
            _data: 2D array of the sample data
            _sample_id: string, identifies the sample in the output file.
            _classification: array (1 x num_classes) of classification [0.0, 1.0]
            _duration: duration in time of the data
        """
        self.data = numpy.array(_data)
        self.sample_id = _sample_id
        self.orig_classification = numpy.array(_classification)
        self.classification = numpy.array(_classification)        
        self.num_classes = len(_classification)
        self.duration = _duration

    def __str__(self):
        return 'ID: ' + str(self.sample_id) + '\n' + 'Data size: ' + str(self.data.shape) + '\n' + \
        'Classification: ' + str(self.classification) + '\n' + 'Original classification: ' + str(self.orig_classification)
        
    def is_valid(self):
        if len(self.sample_id) == 0:
            return False
        if len(self.classification) != self.num_classes:
            return False
        if len(self.orig_classification) != self.num_classes:
            return False
        if numpy.isnan(self.data).any():
            return False
        if numpy.isinf(self.data).any():
            return False
        return True
        
    def get_data(self):
        return self.data

    def get_data_length(self):
        return self.data.size
        
    def pad_data_to_length(self,  _new_length):
        """
            Pads the data matrix until it reaches the _new_length. 
            The padding should avoid changing the information and considering 
            that the data represent an audio file, the padding is executed adding 
            to the right of the matrix  columns from the left.
        """
        old_length = self.data.size
        if _new_length == old_length:
            return
        row_num = self.data.shape[0]
        assert (_new_length - old_length) % row_num == 0
        add_col_number = (_new_length - old_length) / row_num
        while add_col_number > 0:
            col_number = self.data.shape[1]
            if (add_col_number < col_number):
                self.data = numpy.hstack((self.data,  self.data[:,  0:add_col_number]))
                add_col_number = 0
            else:
                self.data = numpy.hstack((self.data,  self.data))
                add_col_number -= col_number

    def get_sample_id(self):
        return self.sample_id

    def reset_classification_to_original(self):
        self.classification = self.orig_classification.copy()
        
    def set_classification(self, _classifications):
        if len(_classifications) == self.num_classes:
            self.classification = numpy.array(_classifications)

    def get_classification(self):
        return self.classification

    def evaluate_classification(self):
        if len(self.classification) != len(self.orig_classification) or len(self.classification) == 0:
            return 0.0
        difference = self.orig_classification - self.classification
        difference_squared = difference * difference
        average = difference_squared.mean()
        return math.sqrt(average)

	def get_duration(self, _duration):
		return self.duration

    def get_num_classes(self):
        return self.num_classes

    def serialize(self,  _separator):
        rv = []
        for idx, probability in enumerate(self.classification):
            line = self.sample_id + str(idx + 1) + str(_separator) + str(probability) + '\n'
            rv.append(line)
        return rv
